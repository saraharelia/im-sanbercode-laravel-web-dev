 
    @extends('layout.master')
    @section('title')
        Halaman Utama
    @endsection
    @section('sub-title')
        Biodata
    @endsection   

    @section('content')
    <form action="/welcome" method="post">
        @csrf
        <!-- <h1>Buat Account Baru!</h1>
        <h2>Sign Up Form</h2> -->
        <label>First name</label><br>
        <input type="text" name="fname"><br><br>
        <label>Last name</label><br>
        <input type="text" name="Lname"><br><br>
        <label>Gender</label><br>
        <input type="radio" name="Gender" value="1">Male <br>
        <input type="radio" name="Gender" value="2">Female <br>
        <input type="radio" name="Gender" value="3">Others <br><br>
        <label>Nationality</label><br>
        <select name="Nationality" id="">
            <option value="1">Indonesia</option>
            <option value="2">Malaysia</option>
            <option value="3">Singapura</option>
            <option value="3">Others</option>
        </select><br> <br>
        <label>Language Spoken</label><br>
        <input type="checkbox" value="1" name="Language">Bahasa Indonesia <br>
        <input type="checkbox" value="2" name="Language">English <br>
        <input type="checkbox" value="3" name="Language">Others <br><br>
        <label>Bio</label><br><br>
        <textarea name="Bio" cols="20" rows="5"></textarea><br>


        <input type="submit" value="Sing Up">
    </form>
    @endsection