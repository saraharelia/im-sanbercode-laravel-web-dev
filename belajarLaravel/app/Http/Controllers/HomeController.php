<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function utama(){  
        return view('welcome');
    }
    public function send(Request $request){
       $firstname = $request['fname'];
       $lastname = $request['Lname'];
       return view('home',['firstname' => $firstname, 'lastname' => $lastname]);
    }
}
