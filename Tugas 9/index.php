<?php
require_once('animals.php');
require_once('Frog.php');
require_once('Ape.php');

$sheep = new animals("sheep");

echo "Name : " . $sheep->name . "<br>"; // "shaun"
echo "Legs : " . $sheep->Leg . "<br>";// 4
echo "cold_blooded : "  . $sheep->cold_blooded . "<br><br>"; // "no"


$kodok = new Frog("buduk");

echo "Name : " . $kodok->name . "<br>"; // "buduk"
echo "Legs : " . $kodok->Leg . "<br>";// 4
echo "cold_blooded : "  . $kodok->cold_blooded . "<br>"; // "no"
echo $kodok->jump() ; // "hop hop"


$sungkong = new Ape("kera sakti");

echo "Name : " . $sungkong->name . "<br>";
echo "Legs : " . $sungkong->Leg . "<br>";// 2
echo "cold_blooded : "  . $sungkong->cold_blooded . "<br>"; // "no"
echo $sungkong->Yell() ; 


?>