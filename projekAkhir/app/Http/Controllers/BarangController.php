<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BarangController extends Controller
{
    public function create(){
        return view('barang.tambah');
    }

   public function store(Request $request)
   {
    $request->validate([
        'nama' => 'required',
        'harga' => 'required',
        'stok' => 'required',
    ]);

    DB::table('barang')->insert([
        'nama' => $request['nama'],
        'harga' => $request['harga'], 
        'stok' => $request['stok']
    ]);

    return redirect('/barang');

   }
   public function index(){
    $barang = DB::table('barang')->get();
    return view('barang.tampil', ['barang' => $barang]);
   }

   public function show($id)
   {
    $barang = DB::table('barang')->where('id', $id)->first();
    return view('barang.detail', ['barang'=> $barang]);
   }
    public function edit($id)
    {
        $barang = DB::table('barang')->where('id', $id)->first(); 
        return view('barang.edit', ['barang'=> $barang]);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'nama' => 'required',
            'harga' => 'required',
            'stok' => 'required'

        ]);

        DB::table('barang')
              ->where('id', $id)
              ->update(
            [
              'nama' => $request->nama,
              'harga' => $request->harga,
              'stok' => $request->stok
            ]
        );
              
        return redirect('/barang');

    }

    public function destroy($id) {
        $query = DB::table('barang')->where('id', $id)->delete();
        return redirect('/barang');
    }

}
