<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\BarangController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',[HomeController::class , 'index']);
Route::get('/Biodata',[AuthController::class , 'bio']);
Route::post('/welcome',[HomeController::class , 'send']);

Route::get('/data-table', function(){
    return view('partials.data-table');
}); 
Route::get('/table', function(){
    return view('partials.table');
}); 

// CRUD barang
// create data
//rute untuk mengarah ke halaman form input barang
Route::get('/barang/create', [BarangController::class, 'create']);
//rute untuk memasukan inputan ke database
Route::post('/barang', [BarangController::class, 'store']);
//Read Data
Route::get('/barang', [BarangController::class, 'index']);

//update data
Route::get('/barang/{barang_id}/edit', [BarangController::class, 'edit']);
//update data ke database berdasarkan id
Route::put('/barang/{barang_id}', [BarangController::class, 'update']);
//Delete
Route::delete('/barang/{barang_id}', [BarangController::class, 'destroy']);

Auth::routes();


