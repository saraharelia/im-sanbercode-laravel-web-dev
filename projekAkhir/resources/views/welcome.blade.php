@extends('layout.master')
    @section('title')
    @endsection
    @section('sub-title')
        DASHBOARD
    @endsection

    @section('content')
    <!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <title>Hello, world!</title>
  </head>
  <body>
        <!-- nav1 -->
        <ul class="nav justify-content-end container-fluid bg-secondary">
            <li class="nav-item">
              <a class="nav-link active text-light" href="#">Gift Card</a>
            </li>
            <li class="nav-item">
              <a class="nav-link text-light" href="#">Track Order</a>
            </li>
            <li class="nav-item">
              <a class="nav-link text-light" href="#">Contact Us</a>
            </li>
          </ul>
          <!-- Penutup nav1 -->
          <!-- nav2 -->
          <nav class="navbar navbar-light bg-secondary">
            <a class="navbar-brand"><img src="https://assets.bukalapak.com/sigil/bukalapak-logo-primary.svg" alt=""></a>
            <form class="form-inline">
              <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
              <button class="btn btn-outline-success my-2 my-sm-0" type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
            </form>
          </nav>
          <!-- penutup nav2 -->
          <!-- nav3 -->
          <ul class="nav justify-content-center m-3 my-2 bg-secondary">
            <li class="nav-item">
              <a class="nav-link active text-light mr-5" href="#">HOME</a>
            </li>
            <li class="nav-item">
              <a class="nav-link text-light mr-5" href="#">SHOP</a>
            </li>
            <li class="nav-item">
              <a class="nav-link text-light mr-5" href="#">BLOG</a>
            </li>
            <li class="nav-item">
                <a class="nav-link text-light mr-5" href="#">ABOUT US</a>
              </li>
          </ul>
          <!-- penutup nav3 -->
          <header>
            <main class="container-fluid">
                <div class="row">
                    <!-- kolom1 -->
                      <div class="col-2">
                        <ul class="list-group">
                            <li class="list-group-item">Hanphone</li>
                            <li class="list-group-item">Laptop</li>
                            <li class="list-group-item">Komputer</li>
                            <li class="list-group-item">Kamera</li>
                            <li class="list-group-item">Elektronik</li>
                            <li class="list-group-item">Sepeda</li>
                            <li class="list-group-item">Aksesoris</li>
                          </ul>
                      </div>
                    <!-- kolom2 -->
                    <div class="col-8">
                        <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                            <div class="carousel-inner">
                              <div class="carousel-item active">
                                <img src="https://s4.bukalapak.com/cinderella/ad-inventory/63fc1a61643c27b3782ac61e/w-1344/DESK_Home_Banner_2018%20%5B1668x704%5D%20(2)-1677466185154.jpg.webp" class="d-block w-100" alt="...">
                              </div>
                              <div class="carousel-item">
                                <img src="https://s4.bukalapak.com/cinderella/ad-inventory/63ff1c942c1a63319dc315b3/w-1344/DESK_Home_Banner_2018_%5B1668x704%5D%20(4)-1677663379987.jpeg.webp" class="d-block w-100" alt="...">
                              </div>
                              <div class="carousel-item">
                                <img src="https://s4.bukalapak.com/cinderella/ad-inventory/63fc3d2cebdf3c0527f348ba/w-1344/DESK_Home_Banner_2018-1677475114759.jpg.webp" class="d-block w-100" alt="...">
                              </div>
                            </div>
                            <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                              <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                              <span class="sr-only">Previous</span>
                            </a>
                            <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                              <span class="carousel-control-next-icon" aria-hidden="true"></span>
                              <span class="sr-only">Next</span>
                            </a>
                          </div>
                          <!-- item -->
                          <div class="row my-3">
                            <div class="col">
                                <div class="card">
                                    <img src="https://images.tokopedia.net/img/cache/200-square/VqbcmM/2021/1/20/5b6654a2-4317-44ba-90f7-6333fa0f7bda.jpg" class="card-img-top" alt="...">
                                    <div class="card-body">
                                      <h4 class="card-title">Apple Macbook Air M1 256 GB</h>
                                      <h5 class="card-title">Rp.23.000.000,00</h5>
                                      <p class="card-text">Apple Macbook Pro M2 2022 13 inch - Garansi Resmi iBox Apple Indonesia - 8/256 GB, Silver</p>
                                      <a href="#" class="btn btn-primary"><i class="fa fa-shopping-cart" aria-hidden="true"></i></a>
                                      <a href="barang/create" class="btn btn-primary btn">Buy</a>
                                    </div>
                                  </div>
                            </div>
                            <div class="col">
                                <div class="card">
                                    <img src="https://images.tokopedia.net/img/cache/200-square/VqbcmM/2023/2/15/5e89831a-e8a9-4f20-af9c-c701c347d1c3.jpg" class="card-img-top" alt="...">
                                    <div class="card-body">
                                      <h4 class="card-title">Apple iPhone 14 Pro Max</h4>
                                      <h5 class="card-title">Rp.25.000.000,00</h5>
                                      <p class="card-text">Apple iPhone 14 Pro Max Garansi Resmi - 128GB 256GB 512GB 1TB Promax - 256GB, Space Black</p>
                                      <a href="#" class="btn btn-primary"><i class="fa fa-shopping-cart" aria-hidden="true"></i></a>
                                      <a href="#" class="btn btn-primary"><i>Buy</i></a>
                                    </div>
                                  </div>
                            </div>
                            <div class="col">
                                <div class="card">
                                    <img src="https://images.tokopedia.net/img/cache/200-square/VqbcmM/2023/3/3/da325044-6f71-456e-a6cc-62e02ba86e51.jpg" class="card-img-top" alt="...">
                                    <div class="card-body">
                                      <h4 class="card-title">Samsung Galaxy S22</h4>
                                      <h5 class="card-title">Rp.1.000.000,00</h5>
                                      <p class="card-text">Baru Galaxy S22 Ultra hp murah Handphone 12GB + 512GB Smartphone 5G - Hitam, 6GB+128GB</p>
                                      <a href="#" class="btn btn-primary"><i class="fa fa-shopping-cart" aria-hidden="true"></i></a>
                                      <a href="#" class="btn btn-primary"><i>Buy</i></a>
                                    </div>
                                  </div>
                            </div>
                          </div>
                      </div>
                    <!-- kolom3 -->
                    <div class="col-2">
                        <ul class="list-group">
                            <li class="list-group-item">Iphone 11</li>
                            <li class="list-group-item">Iphone 12</li>
                            <li class="list-group-item">Macbook</li>
                            <li class="list-group-item">Samsung S5</li>
                            <li class="list-group-item">Samsung S6</li>
                          </ul>
                          <!-- externalform -->
                          <form class="mt-3">
                            <div class="form-group ">
                              <label for="exampleInputEmail1">Email address</label>
                              <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                              <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                            </div>
                            <div class="form-group">
                              <label for="exampleInputPassword1">Password</label>
                              <input type="password" class="form-control" id="exampleInputPassword1">
                            </div>
                            <div class="form-group form-check">
                              <input type="checkbox" class="form-check-input" id="exampleCheck1">
                              <label class="form-check-label" for="exampleCheck1">Check me out</label>
                            </div>
                            <button type="submit" class="btn btn-primary">Submit</button>
                          </form>
                      </div>
                    </div>  
            </main>
          </header>
   

    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: jQuery and Bootstrap Bundle (includes Popper) -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>

    <!-- Option 2: jQuery, Popper.js, and Bootstrap JS
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js" integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s" crossorigin="anonymous"></script>
    -->
  </body>
</html>
 @endsection
