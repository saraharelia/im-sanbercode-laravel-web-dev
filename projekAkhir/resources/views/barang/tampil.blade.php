@extends('layout.master')
    @section('title')
        Halaman Tampil Barang
    @endsection
    @section('sub-title')
        Halaman Barang
    @endsection 
    @section('content')

    <a href="barang/create" class="btn btn-primary btn-sm">Tambah Data</a>
    <table class="table">
    <thead>
    <tr>
      <th scope="col">No</th>
      <th scope="col">Nama Barang</th>
      <th scope="col">Harga</th>
      <th scope="col">Stok</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody>
    @forelse ($barang as $key => $value)
    <tr>
      <th scope="row">{{$key + 1}}</th>
      <td>{{$value->nama}}</td>
      <td>{{$value->harga}}</td>
      <td>{{$value->stok}}</td>
      <td> 
                <form action="/barang/{{$value->id}}" method="POST">
                    @csrf 
                    @method('DELETE')
                    <a href="/barang/{{$value->id}}" class="btn btn-info btn-sm">Detail</a>
                    <a href="/barang/{{$value->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                    <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                    
                </form>
            </td>
            
      
    </tr>
    @empty
    <h1>Data Koseng</h1>
    @endforelse
    
  </tbody>
</table>
    @endsection 