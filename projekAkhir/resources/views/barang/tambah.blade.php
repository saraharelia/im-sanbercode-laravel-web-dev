@extends('layout.master')
    @section('title')
        Halaman Tambah Barang
    @endsection
    @section('sub-title')
        Halaman Barang
    @endsection 
    @section('content')
    <form action="/barang" method="POST">
    @csrf
    <div class="form-group">
    <label>Nama Barang</label>
    <input type="text" name="nama" class="form-control">
    </div>
    @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
    <label>Harga</label>
    <input type="number" name="harga" class="form-control">
    </div>
    @error('harga')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
    <label>Stok</label>
    <input type="number" name="stok" class="form-control">
    </div>
    @error('stok')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
    </form>
    @endsection